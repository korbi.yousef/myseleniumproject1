@search
Feature: Search product on amazon
  To conduct a product search on Amazon and verify that the search results match the products I was looking for

  @tag1
  Scenario: Search products
    Given I want browser is launched and site search page is displayed 
    When I set "dell" on the search bar
    And I click on search button 
    Then I validate the product displayed are "dell" producs