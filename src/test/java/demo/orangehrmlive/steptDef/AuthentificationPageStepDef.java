package demo.orangehrmlive.steptDef;

import java.time.Duration;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait; // Import the correct WebDriverWait class
import demo.orangehrmlive.pageObjects.AuthentificationPage;
import demo.orangehrmlive.pageObjects.HomePage;
import demo.orangehrmlive.utils.Setup;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class AuthentificationPageStepDef {
    public WebDriver driver;
    public AuthentificationPage authentificationPage;
    public HomePage homePage;
    final static String URL = "https://opensource-demo.orangehrmlive.com/";
    public WebDriverWait wait;
    public AuthentificationPageStepDef() {
        driver = Setup.driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(30));
    }
    @Given("I am on the login page")
    public void i_am_on_the_login_page() throws InterruptedException {
        // This step is already handled in the BaseTest setup
        driver.get(URL);
        authentificationPage = new AuthentificationPage(driver);
        PageFactory.initElements(driver, AuthentificationPage.class);
    	wait.until(ExpectedConditions.visibilityOf(authentificationPage.userPWD));
    	Assert.assertTrue(authentificationPage.userPWD.isDisplayed());
    }
    @When("I set the ID")
    public void i_set_the_id() {
        // Fill in the username
        authentificationPage.iSetTheID("Admin");
    }
    @When("I set the PWD")
    public void i_set_the_pwd() {
        // Fill in the password
        authentificationPage.iSetThePWD("admin123");
    }
    
    @Then("I validate the DASHBOARD is displayed")
    public void i_validate_the_dashboard_is_displayed() throws InterruptedException {
        // Click on the submit button
        authentificationPage.clickSubmitBtn();
        homePage = new HomePage(driver);
        PageFactory.initElements(driver, HomePage.class);
        wait.until(ExpectedConditions.visibilityOf(homePage.logoIMG));
        Assert.assertTrue(homePage.logoIMG.isDisplayed());
        System.out.println("Succeffuly authentified");
    }
}
