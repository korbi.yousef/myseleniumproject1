package demo.orangehrmlive.steptDef;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import demo.orangehrmlive.pageObjects.AmazonSearchPage;
import demo.orangehrmlive.pageObjects.SearchPageResult;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class AmazonSearchPageStepDefs {

	String URL = "https://www.amazon.fr/";
	WebDriver driver;
	AmazonSearchPage amazonSearchPage;
	SearchPageResult searchPageResult;
	@Given("I want browser is launched and site search page is displayed")
	public void i_want_browser_is_launched_and_site_search_page_is_displayed() throws InterruptedException {
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		driver.get(URL);
		
	}

	@When("I set {string} on the search bar")
	public void i_set_on_the_search_bar(String product) {
		amazonSearchPage = new AmazonSearchPage(driver);
		amazonSearchPage.toAcceptCookies();
		amazonSearchPage.SetSearchBox(product);
	}

	@When("I click on search button")
	public void i_click_on_search_button() {
		amazonSearchPage.clickSearchBtn();
	}

	@Then("I validate the product displayed are {string} producs")
	public void i_validate_the_product_displayed_are_producs(String productName) throws InterruptedException {
		searchPageResult = new SearchPageResult(driver);
		searchPageResult.ValidateProductList(productName);
		driver.quit();
	}

}
