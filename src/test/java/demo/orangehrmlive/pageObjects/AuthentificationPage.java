package demo.orangehrmlive.pageObjects;

import java.time.Duration;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

public class AuthentificationPage {

    final static String USER_NAME_NAME = "username";
    final static String USER_PWD_NAME = "password";  
    final static String SUBMIT_BTN_XPATH = "//button[@type = 'submit']";
    final static String LOGO_IMG_XPATH = "//img[@alt = 'client brand banner']";
    
    /* @FindBy */
    @FindBy(how = How.NAME, using = USER_NAME_NAME)
    public WebElement userName;

    @FindBy(how = How.NAME, using = USER_PWD_NAME)
    public WebElement userPWD;

    @FindBy(how = How.XPATH, using = SUBMIT_BTN_XPATH) // Corrected locator strategy
    public WebElement submitBtn;

    @FindBy(how = How.XPATH, using = LOGO_IMG_XPATH) // Corrected locator strategy
    public WebElement logoIMG;

    public AuthentificationPage(WebDriver driver) {
        PageFactory.initElements(driver, this); // Initialize elements using Page Factory
    }

    
    public void iSetTheID(String name) {
        userName.sendKeys(name);
    }

    public void iSetThePWD(String PWD) {
    	userPWD.sendKeys(PWD);
    }

    public void clickSubmitBtn() {
    	submitBtn.click();
    }
}
