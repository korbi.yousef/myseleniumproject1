package demo.orangehrmlive.pageObjects;

import java.time.Duration;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchPageResult {

	WebDriver driver ;
	WebDriverWait wait; 
	
	private By result_item = By.xpath("//h2[@class ='a-size-mini a-spacing-none a-color-base s-line-clamp-4']");

	
	public SearchPageResult(WebDriver driver) {
		this.driver = driver;
	}
	
	public void ValidateProductList(String text) {
		wait = new WebDriverWait(driver, Duration.ofSeconds(30));
		WebElement resultItem = wait.until(ExpectedConditions.elementToBeClickable(result_item));
		Assert.assertTrue(resultItem.getText().toLowerCase().contains(text));
	}

}
