package demo.orangehrmlive.pageObjects;

import java.time.Duration;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

public class HomePage {

    final static String LOGO_IMG_XPATH = "//img[@alt = 'client brand banner']";
    
    /* @FindBy */
    @FindBy(how = How.XPATH, using = LOGO_IMG_XPATH) // Corrected locator strategy
    public WebElement logoIMG;

    public HomePage(WebDriver driver) {
        PageFactory.initElements(driver, this); // Initialize elements using Page Factory
    }
}
