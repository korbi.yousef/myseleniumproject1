package demo.orangehrmlive.pageObjects;

import java.time.Duration;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AmazonSearchPage {

	WebDriver driver;
	WebDriverWait wait;

	private By cookies_Box = By.id("sp-cc-accept");
	private By search_text_Box = By.id("twotabsearchtextbox");
	private By search_btn = By.id("nav-search-submit-button");
	private By result_item = By.xpath("//h2[@class ='a-size-mini a-spacing-none a-color-base s-line-clamp-4']");

	public AmazonSearchPage(WebDriver driver) {
		this.driver = driver;
	}

	public void toAcceptCookies() {
		wait = new WebDriverWait(driver, Duration.ofSeconds(30));
		WebElement cookiesBox = wait.until(ExpectedConditions.elementToBeClickable(cookies_Box));
		cookiesBox.click();

	}

	public void SetSearchBox(String text) {

		WebElement searchTextBox = wait.until(ExpectedConditions.elementToBeClickable(search_text_Box));
		searchTextBox.sendKeys(text);
	}

	public void clickSearchBtn() {
		driver.findElement(search_btn).click();
	}

}