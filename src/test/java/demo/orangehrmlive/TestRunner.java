package demo.orangehrmlive;

import org.junit.runner.RunWith;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features= "src/specs/features/authentification",
		tags="@search",
				plugin = {
				     
				        "html:target/cucumber-html-report",
				        "json:target/cucumber.json",
				 }
		)
public class TestRunner {
	    						
}