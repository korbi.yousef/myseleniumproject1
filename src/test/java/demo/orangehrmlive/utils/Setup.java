package demo.orangehrmlive.utils;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.FluentWait;

import io.cucumber.java.After;
import io.cucumber.java.Before;

public class Setup {

	public static WebDriver driver;
	static DriverManager driverManager;
	public static FluentWait<WebDriver> wait; // Use FluentWait<WebDriver> instead of Wait<WebDriver>

	@Before
	/**
	 * Call browser with the design pattern factory navigator *
	 */
	public static void setup() throws InterruptedException {
		driver = new ChromeDriver();
//		wait = new FluentWait<>(driver).withTimeout(Duration.ofSeconds(60)).pollingEvery(Duration.ofSeconds(1))
//				.ignoring(Exception.class);
	}
	
	@After 
	public void tearDown () {
		driver.quit();
	}
	
	
}
